importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js');
firebase.initializeApp({
  apiKey: "AIzaSyDY5AmBo67h7E3icrxY_zQ4MG5TxHeOEJQ",
    authDomain: "fhs-ed-iot.firebaseapp.com",
    projectId: "fhs-ed-iot",
    storageBucket: "fhs-ed-iot.appspot.com",
    messagingSenderId: "587287114726",
    appId: "1:587287114726:web:8d155c374705a38739dc27"
});
const messaging = firebase.messaging();
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('../firebase-messaging-sw.js')
    .then(function (registration) {
      console.log("Service Worker Registered");
      messaging.useServiceWorker(registration);
    });
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/firebase-messaging-sw.js');
  });
  // Get registration token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken().then((currentToken) => {
    if (currentToken) {
      // Send the token to your server and update the UI if necessary
      // ...
    } else {
      // Show permission request UI
      console.log('No registration token available. Request permission to generate one.');
      // ...
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
  });
  messaging.onBackgroundMessage(function (payload) {
    console.log('Received background message ', payload);
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
    };

    self.registration.showNotification(notificationTitle,
      notificationOptions);
  });

  self.addEventListener('notificationclick', function (event) {
    console.log('notification received: ', event)
  });
}


