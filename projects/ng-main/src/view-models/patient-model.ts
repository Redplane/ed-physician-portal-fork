export class PatientModel {
  id: string;
  patient_info: {
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    ip_address: string;
  };
}
