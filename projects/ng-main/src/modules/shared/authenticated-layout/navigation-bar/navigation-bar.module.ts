import {NgModule} from '@angular/core';
import {NavigationBarComponent} from './navigation-bar.component';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [
        NavigationBarComponent
    ],
    imports: [
        RouterModule
    ],
    exports: [
        NavigationBarComponent
    ]
})
export class NavigationBarModule {

}
