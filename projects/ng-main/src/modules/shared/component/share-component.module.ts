import { CommonModule } from '@angular/common';
import {NgModule} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastsContainerComponent } from './toast/toast-container.component';


@NgModule({
  imports: [NgbModule, CommonModule],
  declarations: [
    ToastsContainerComponent,
  ],
  exports: [
    ToastsContainerComponent,
  ]
})
export class ShareComponentModule {}
