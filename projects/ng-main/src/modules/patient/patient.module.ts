import {NgModule} from '@angular/core';
import {PatientRouteModule} from './patient.route';

//#region Routes declaration


//#endregion

//#region Module declaration
@NgModule({
  imports: [
    PatientRouteModule
  ]
})

export class PatientModule {
}

//#endregion
