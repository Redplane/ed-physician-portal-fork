import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { IDayCalendarConfig, DatePickerComponent } from 'ng2-date-picker';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['date-range-picker.component.scss']
})
export class DateRangePickerComponent implements OnInit {
  @ViewChild('dateFromDp') public dateFromDp: DatePickerComponent;
  @ViewChild('dateToDp') public dateToDp: DatePickerComponent;

  public filterForm: FormGroup;
  public displayDate;
  public dayPickerConfig = <IDayCalendarConfig>{
    locale: 'en',
    format: 'MM/DD/YYYY hh:mm:ss',
    monthFormat: 'MMMM, YYYY',
    firstDayOfWeek: 'mo',
  };

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  public ngOnInit(): void {
    // When DateFrom changes we set the min selectable value for DateTo
    this.filterForm.get('dateFrom').valueChanges.subscribe(value => {
      this.dateToDp.displayDate = value; // DateTo
      this.dayPickerConfig = {
        // min: value,
        ...this.dayPickerConfig
      };
    });
  }

  private createForm(): void {
    this.filterForm = this.fb.group({
      dateFrom: new FormControl(),
      dateTo: new FormControl(),
    });
  }
}
