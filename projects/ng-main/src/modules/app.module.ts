import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRouteModule } from './app.route';
import { AppConfigService } from '../services/implementations/app-config.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../factories/ngx-translate.factory';
import { HttpClient } from '@angular/common/http';
import { NgRxMessageBusModule } from 'ngrx-message-bus';
import { appConfigServiceFactory } from '../factories/app-setting.factory';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment.dev';
import { AsyncPipe } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MessagingService } from '../services/implementations/messaging.service';
import { ShareComponentModule } from './shared/component/share-component.module';
//#region Module declaration

@NgModule({
  declarations: [],
  imports: [
    AppRouteModule,
    // Message bus registration.
    NgRxMessageBusModule.forRoot(),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

  ],
  providers: [
    AppConfigService,
    MessagingService,
    AsyncPipe,
    {
      provide: APP_INITIALIZER,
      useFactory: appConfigServiceFactory,
      multi: true,
      deps: [AppConfigService]
    }
  ],
  bootstrap: [AppComponent]
})


export class AppModule {
}

//#endregion
