import {Inject, Injectable} from '@angular/core';
import {IPatientService} from '../interfaces/patient-service.interface';
import {HttpClient} from '@angular/common/http';
import {API_ENDPOINT_RESOLVER} from '../../constants/injection-token.constant';
import {IApiEndPointResolver} from '../interfaces/api-end-point.resolver';
import {Observable} from 'rxjs';
import {PatientModel} from '../../view-models/patient-model';
import {ModuleNameConstant} from '../../constants/module-name.constant';
import {map, mergeMap} from 'rxjs/operators';
import {HeartBeatModel} from '../../view-models/heart-beat.model';

@Injectable()
export class PatientService implements IPatientService {
  constructor(@Inject(API_ENDPOINT_RESOLVER) protected endPointResolver: IApiEndPointResolver,
              public httpClient: HttpClient) {
  }

  // Load account module end point.
  protected getModuleEndpointAsync(): Observable<string> {
    return this.endPointResolver
      .getEndPointAsync(ModuleNameConstant.patient);
  }

  public getPatientsAsync(): Observable<PatientModel[]> {
    return this.getModuleEndpointAsync()
      .pipe(
        mergeMap((patientEndpoint: string) => {
          return this.httpClient.get(`${patientEndpoint}`)
            .pipe(
              map((response: any) => {
                const result = [];
                if (response && response.Items && response.Items.length > 0) {
                  response.Items.forEach(patient => {
                    result.push(patient as PatientModel);
                  });
                }
                return result;
              })
            );
        })
      );
  }

  public getPatientDetailsByIdAsync(patientId: string): Observable<PatientModel> {
    return this.getModuleEndpointAsync()
      .pipe(
        mergeMap((patientEndpoint: string) => {
          return this.httpClient.get(`${patientEndpoint}/${patientId}`);
        }),
        map((response: any) => {
          const patient = new PatientModel();
          patient.id = patientId;
          // @ts-ignore
          patient.patient_info = {};
          patient.patient_info.first_name = response.first_name;
          patient.patient_info.email = response.email;
          patient.patient_info.gender = response.gender;
          patient.patient_info.last_name = response.last_name;
          patient.patient_info.ip_address = response.ip_address;
          return patient;
        }),
      );
  }

  public getHeartbeatsDataAsync(patientId: string, startDate: string, endDate: string): Observable<HeartBeatModel[]> {
    return this.getModuleEndpointAsync()
      .pipe(
        mergeMap((patientEndpoint: string) => {
          const param = startDate && endDate ? `heartbeats?startDate=${startDate}&endDate=${endDate}` : `heartbeats`;
          return this.httpClient.get<HeartBeatModel[]>(`${patientEndpoint}/${patientId}/${param}`);
        })
      );
  }
}
